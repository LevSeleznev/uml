package observer;

/**
 * Наблюдатели могут относиться к любому классу, реализующему интерфейс Observer.
 * Каждый наблюдатель регестрируется у конкретного субъекта для получения обновлений.
 *
 * @author Lev Seleznev
 * @version 1.0
 */
public class StatisticsDisplay implements Observer, DisplayElement {
    private Subject observable;
    private int temperature;
    private int humidity;
    private int pressure;

    StatisticsDisplay(Subject subject) {
        observable = subject;
        subject.registerObserver(this);
    }

    @Override
    public void display() {
        System.out.println("StatisticsDisplay. Temperature: " + temperature + ". Humidity: " + humidity + ". Pressure: " + pressure);
    }

    @Override
    public void update(int temperature, int humidity, int pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this. pressure = pressure;
        display();
    }
}
