package observer;

import java.util.ArrayList;

/**
 * Субъект реализует интерфейс Subject.
 *
 * @author Lev Seleznev
 * @version 1.0
 */
public class WeatherData implements Subject {
    private ArrayList<Observer> observers;
    private int temperature;
    private int humidity;
    private int pressure;

    WeatherData() {
        observers = new ArrayList<>();
    }

    @Override
    public void registerObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        int index = observers.indexOf(o);
        if (index > 0) {
            observers.remove(index);
        }
    }

    @Override
    public void notifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = observers.get(i);
            observer.update(temperature, humidity, pressure);
        }
    }

    public void changeData() {
        notifyObservers();
    }

    public void setChanged(int temperature, int humidity, int pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        changeData();
    }
}
