package observer;

/**
 * Используется объектами для регистрации в качестве наблюдателя
 *
 * @author Lev Seleznev
 * @version 1.0
 */
public interface Subject {
    void registerObserver(Observer o);

    void removeObserver(Observer o);

    void notifyObservers();
}
