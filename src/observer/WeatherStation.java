package observer;

/**
 * @author Lev Seleznev
 * @version 1.0
 */
public class WeatherStation {
    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData();
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        CurrentConditionsDisplay currentConditionsDisplay = new CurrentConditionsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.setChanged(30, 100, 40);
        weatherData.setChanged(50, 60, 10);
        weatherData.setChanged(10, 80, 20);
    }
}
