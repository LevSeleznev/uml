package observer;

/**
 * Каждый визуальный элемент должен реализовать метод display
 *
 * @author Lev Seleznev
 * @version 1.0
 */
public interface DisplayElement {
    void display();
}
