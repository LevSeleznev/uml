package observer;

/**
 * Каждый потенциальный наблюдатель должен реализовать данный интерфейс
 *
 * @author Lev Seleznev
 * @version 1.0
 */
public interface Observer {
    void update(int temperature, int humidity, int pressure);
}
