package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class NYStyleCheesePizza extends Pizza {
    NYStyleCheesePizza() {
        name = "Cheese Pizza";
    }

    public void create() {
        System.out.println("Create NY Cheese Pizza");
    }
}
