package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class ChicagoStyleCheesePizza extends Pizza {
    ChicagoStyleCheesePizza() {
        name = "Cheese Pizza";
    }

    public void create() {
        System.out.println("Create Chicago Cheese Pizza");
    }
}
