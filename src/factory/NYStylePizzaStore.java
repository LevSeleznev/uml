package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class NYStylePizzaStore extends PizzaStore {
    @Override
    public Pizza create(String type) {
        Pizza pizza = null;
        if(type.equals("cheese")) {
            pizza = new NYStyleCheesePizza();
        } else if(type.equals("pepperoni")) {
            pizza = new NYStylePepperoniPizza();
        }
        return pizza;
    }
}
