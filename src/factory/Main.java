package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class Main {
    public static void main(String[] args) {
        PizzaStore pizzaStoreNY = new NYStylePizzaStore();
        Pizza nyCheesePizza = pizzaStoreNY.order("cheese");
        System.out.println("I'm eating " + nyCheesePizza.getName());

        PizzaStore pizzaStoreChicago = new ChicagoStylePizzaStore();
        Pizza chicagoPepperoniPizza = pizzaStoreChicago.order("pepperoni");
        System.out.println("I'm eating " + chicagoPepperoniPizza.getName());
    }
}
