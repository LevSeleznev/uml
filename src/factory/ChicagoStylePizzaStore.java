package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class ChicagoStylePizzaStore extends PizzaStore {
    @Override
    public Pizza create(String type) {
        Pizza pizza = null;
        if(type.equals("cheese")) {
            pizza = new ChicagoStyleCheesePizza();
        } else if(type.equals("pepperoni")) {
            pizza = new ChicagoStylePepperoniPizza();
        }
        return pizza;
    }
}
