package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class ChicagoStylePepperoniPizza extends Pizza {
    ChicagoStylePepperoniPizza() {
        name = "Pepperoni Pizza";
    }

    public void create() {
        System.out.println("Create Chicago Pepperoni Pizza");
    }
}
