package factory;

/**
 * Created by Proger on 28.01.2016.
 */
abstract public class PizzaStore {
    public Pizza order(String type) {
        Pizza pizza = create(type);
        pizza.create();
        pizza.box();
        return pizza;
    }

    abstract public Pizza create(String type);
}
