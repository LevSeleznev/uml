package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public class NYStylePepperoniPizza extends Pizza {
    NYStylePepperoniPizza() {
        name = "Pepperoni Pizza";
    }

    public void create() {
        System.out.println("Create NY Pepperoni Pizza");
    }
}
