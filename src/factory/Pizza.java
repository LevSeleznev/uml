package factory;

/**
 * Created by Proger on 28.01.2016.
 */
public abstract class Pizza {
    String name;

    public String getName() {
        return name;
    }

    public void create() {
        System.out.println("Create pizza");
    }

    public void box() {
        System.out.println("Place pizza in official PizzaStore box");
    }
}
