package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
public class Ship extends Decorator {
    Component component;

    Ship(Component component) {
        this.component = component;
    }

    @Override
    public String getDescription() {
        return component.getDescription() + ", Ship";
    }

    @Override
    public double cost() {
        return component.cost() + .40;
    }
}
