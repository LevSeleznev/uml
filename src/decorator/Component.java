package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
abstract class Component {
    protected String description;

    public String getDescription() {
        return description;
    }

    abstract public double cost();
}
