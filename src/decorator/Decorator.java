package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
abstract class Decorator extends Component {
    Component component;
    abstract public String getDescription();
}
