package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
public class DarkRouse extends Component {

    DarkRouse() {
        description = "DarkRouse";
    }

    @Override
    public double cost() {
        return 1.20;
    }
}
