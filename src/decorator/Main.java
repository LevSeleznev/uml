package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
public class Main {
    public static void main(String[] args) {
        Component darkRouse = new DarkRouse();
        darkRouse = new Mocha(darkRouse);
        darkRouse = new Mocha(darkRouse);
        darkRouse = new Ship(darkRouse);
        System.out.println(darkRouse.cost() + " " + darkRouse.getDescription());
    }
}
