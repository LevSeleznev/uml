package decorator;

/**
 * Created by Proger on 28.01.2016.
 */
public class Mocha extends Decorator {

    Mocha(Component component) {
        this.component = component;
    }

    @Override
    public String getDescription() {
        return component.getDescription() + ", Mocha";
    }

    @Override
    public double cost() {
        return component.cost() + .20;
    }
}
