Паттерн Наблюдатель
===================

Диаграмма паттерна наблюдатель, созданная с вомощью plantuml.

```
В ходе выполнения задания столкнулся с тем, что не могу отличить агрегацию от композиции.
```

1. Класс WeatherData содержит массив классов Observer, как я понял, это агрегация.

2. При создании класса Observer ему в конструктор передаётся объект класса WeatherData,
т.к без передачи этого объекта класса не создать, это композиция.


```
Я правильно понял?
```
[UML схема](./Observer.png)